import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class Color_4 extends JFrame{

	private static final int FRAME_WIDTH = 385;
	private static final int FRAME_HEIGHT = 500;
	private JFrame frame;
	private JPanel panel;
	private JPanel background;
	private JComboBox combobox;
	
	
	public void run(){
		frame = new JFrame();
		panel = new JPanel();
		background = new JPanel();
	//	panel.setLayout(new BorderLayout());
		background.setLayout(new BorderLayout());
		createControlPanel();
		frame.add(background);
		frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Color_4");
		frame.setVisible(true);
	}


	public void createControlPanel() {
		// TODO Auto-generated method stub
		
		combobox = new JComboBox();
		combobox.addItem("RED");
		combobox.addItem("GREEN");
		combobox.addItem("BLUE");
		combobox.setEditable(false);
		combobox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String combo = (String) combobox.getSelectedItem();

				if (combo == "RED") {
					background.setBackground(Color.RED);
				}
				else if (combo == "GREEN") {
					background.setBackground(Color.GREEN);
				} else {
					background.setBackground(Color.BLUE);
					
				}
				
			}
		});
		panel.add(combobox);
		background.add(panel,BorderLayout.SOUTH);
		
	}
	

}
