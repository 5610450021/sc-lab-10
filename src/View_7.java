import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class View_7 extends JFrame{

	private static final int FRAME_WIDTH = 480;
	private static final int FRAME_HEIGHT = 428;
	private JFrame frame;
	private JPanel panel;
	private JLabel labelamount;
	private JTextArea amounttext;
	private JLabel labelresult;
	private JTextArea resulttext;
	private JButton depobut;
	private JButton withbut;
	BankAccount account;
	
	public void run(){
		frame = new JFrame();
		panel = new JPanel();
		
		panel.setLayout(new BorderLayout());
		createControlPanel();
		frame.add(panel);
		frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("View_7");
		frame.setVisible(true);
	}


	public void createControlPanel() {
		// TODO Auto-generated method stub
		account = new BankAccount();
		labelamount = new JLabel("Interest Rate : ");
		labelamount.setBounds(14, 20, 89, 23);
		amounttext  = new JTextArea();
		amounttext.setBounds(98, 20, 125, 23);
		labelresult = new JLabel("Balance : ");
		labelresult.setBounds(98, 50, 200, 23);
		resulttext = new JTextArea();
		resulttext.setBounds(14, 80, 440, 290);
		depobut = new JButton("Deposit");
		depobut.setBounds(250, 20, 89, 23);
		depobut.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				double amount = Double.parseDouble(amounttext.getText());
				account.deposit(amount);
				labelresult.setText("Balance : "+account.getBalance());
				resulttext.setText("Balance : "+account.getBalance());
				
			}
		});
		withbut = new JButton("Withdraw");
		withbut.setBounds(350, 20, 89, 23);
		withbut.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				double amount = Double.parseDouble(amounttext.getText());
				account.withdraw(amount);
				labelresult.setText("Balance : "+account.getBalance());
				resulttext.setText("Balance : "+account.getBalance());
			}
		});
		frame.add(resulttext);
		frame.add(labelamount);
		frame.add(amounttext);
		frame.add(labelresult);
		frame.add(depobut);
		frame.add(withbut);
	}
	

}
