import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class Color_5 extends JFrame{

	private static final int FRAME_WIDTH = 385;
	private static final int FRAME_HEIGHT = 500;
	private JFrame frame;
	private JPanel panel;
	private JPanel background;
	private JMenuBar menuBar;
	
	
	public void run(){
		frame = new JFrame();
		panel = new JPanel();
		background = new JPanel();
	//	panel.setLayout(new BorderLayout());
		background.setLayout(new BorderLayout());
		createControlPanel();
		frame.add(background);
		frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Color_5");
		frame.setVisible(true);
	}


	public void createControlPanel() {
		// TODO Auto-generated method stub
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		menuBar.add(createFileMenu());
		menuBar.add(createColorMenu());
		panel.add(menuBar);
		background.add(panel,BorderLayout.SOUTH);
		
	}
	
	public JMenu createFileMenu(){
		JMenu menu = new JMenu("File");
		JMenuItem item = new JMenuItem("Exit");
		item.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
		menu.add(item);
		return menu;
	}

	public JMenu createColorMenu(){
		JMenu menu = new JMenu("Edit");
		JMenu menu2 = new JMenu("Color");
		JMenuItem item = new JMenuItem("Red");
		item.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				background.setBackground(Color.RED);
			}
		});
		JMenuItem item2 = new JMenuItem("Green");
		item2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				background.setBackground(Color.GREEN);
			}
		});
		JMenuItem item3 = new JMenuItem("Blue");
		item3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				background.setBackground(Color.BLUE);
			}
		});
		menu2.add(item);
		menu2.add(item2);
		menu2.add(item3);
		menu.add(menu2);
		return menu;
	}
}
