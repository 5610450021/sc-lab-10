import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class Color_3 extends JFrame{

	private static final int FRAME_WIDTH = 385;
	private static final int FRAME_HEIGHT = 500;
	private JFrame frame;
	private JPanel panel;
	private JPanel background;
	private JCheckBox redbutton;
	private JCheckBox greenbutton;
	private JCheckBox bluebutton;
	
	public void run(){
		frame = new JFrame();
		panel = new JPanel();
		background = new JPanel();
		background.setLayout(new BorderLayout());
		createControlPanel();
		frame.add(background);
		frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Color_3");
		frame.setVisible(true);
	}


	public void createControlPanel() {
		// TODO Auto-generated method stub
		
		
		redbutton = new JCheckBox("RED");
		redbutton.setBounds(50, 400, 80, 30);
		redbutton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				controlButton();
			}
		});
		greenbutton = new JCheckBox("GREEN");
		greenbutton.setBounds(150, 400, 80, 30);
		greenbutton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				controlButton();
			}
		});
		
		bluebutton = new JCheckBox("BLUE");
		bluebutton.setBounds(250, 400, 80, 30);
		bluebutton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				controlButton();
				//frame.repaint();
			}
		});
		
		panel.add(redbutton);
		panel.add(greenbutton);
		panel.add(bluebutton);
		background.add(panel,BorderLayout.SOUTH);
		
	}
	
	public void controlButton(){
		if (redbutton.isSelected()) {
			
			background.setBackground(Color.RED);
					
		} else if (greenbutton.isSelected()){
			
			background.setBackground(Color.GREEN);
					
		}
		else if (bluebutton.isSelected()){
			background.setBackground(Color.BLUE);
				
		}
		else if (redbutton.isSelected() && greenbutton.isSelected()){
			background.setBackground(Color.YELLOW);
		}
		else if (redbutton.isSelected() && bluebutton.isSelected()){
			background.setBackground(new Color(255,0,255));
		}
		else if (greenbutton.isSelected() && bluebutton.isSelected()){
			background.setBackground(new Color(0,255,255));
		}
		else {
			background.setBackground(new Color(255,255,255));
		}
	}
}
