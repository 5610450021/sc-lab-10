import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class Color_2 extends JFrame{

	private static final int FRAME_WIDTH = 385;
	private static final int FRAME_HEIGHT = 500;
	private JFrame frame;
	private JPanel panel;
	private JPanel background;
	private JRadioButton redbutton;
	private JRadioButton greenbutton;
	private JRadioButton bluebutton;
	private ButtonGroup group;
	
	public void run(){
		frame = new JFrame();
		panel = new JPanel();
		background = new JPanel();
	//	panel.setLayout(new BorderLayout());
		background.setLayout(new BorderLayout());
		createControlPanel();
		frame.add(background);
		frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Color_2");
		frame.setVisible(true);
	}


	public void createControlPanel() {
		// TODO Auto-generated method stub
		
		group = new ButtonGroup();
		redbutton = new JRadioButton("RED");
		redbutton.setBounds(50, 400, 80, 30);
		redbutton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				background.setBackground(Color.RED);
				//frame.repaint();
			}
		});
		greenbutton = new JRadioButton("GREEN");
		greenbutton.setBounds(150, 400, 80, 30);
		greenbutton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				background.setBackground(Color.GREEN);
				//frame.repaint();
			}
		});
		bluebutton = new JRadioButton("BLUE");
		bluebutton.setBounds(250, 400, 80, 30);
		bluebutton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				background.setBackground(Color.BLUE);
				//frame.repaint();
			}
		});
		group.add(redbutton);
		group.add(greenbutton);
		group.add(bluebutton);
		panel.add(redbutton);
		panel.add(greenbutton);
		panel.add(bluebutton);
		background.add(panel,BorderLayout.SOUTH);
		
	}
	

}
